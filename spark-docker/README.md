# spark-docker

Spark stand-alone cluster docker container definition.

Is built and run from the root of the project with docker compose:

```bash
docker-compose build # build image
docker-compose up # start cluster (master + 1 worker)
```

Once the cluster is up and running, you can connect to it from the driver machine:

```bash
spark-shell --master spark://localhost:7077
```

Spark master web UI is available at http://localhost:8080.

Source: https://github.com/semberal/spark-docker
