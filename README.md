# DS-BUILDER

Data Source project builder based on gradle and docker-compose.

### Description

The project bundles related _data-source-joiner_ application features and services.  
Each feature is implemented as stand-alone Java application wrapped in the docker container.  
Containers are grouped in the network of services using _docker-compose_.

### Technology stack

* Spark 2.1.0 (docker image, by: [semberal](https://github.com/semberal/spark-docker))
* Elasticsearch 6.5
* spark-jobs-rest-client 1.3.9 (by: [ywilkof](https://github.com/ywilkof/spark-jobs-rest-client))
* Spring Boot 2.0.5
* Gradle build tool
* Docker & Docker Compose

### Overview

#### Applications

* __csv-loader__ - a spark application responsible for generating DS data or loading it from the CSV file.
* __ds-joiner__ - a spark application responsible for joining existing data sources according to specified criteria. 
* __ds-manager__ - a web application that exposes RESTfull API to:
    * define/create a data source (DS) by generating DS data or loading the data from the CSV file.
    * join existing data sources according to the specified criteria (names, columns, join type)
    * inquire submitted job execution status
* __ds-explorer__ - a web application that exposes RESTfull API to:
    * retrieve elasticsearch cluster info
    * retrieve a list of existing data sources (indices)
    * search existing data sources by specified criteria (index, type, value)

#### Services

- __spark__ (version 2.1.0) - a stand-alone spark cluster running in a docker container (default: 1-master + 1-worker).
- __elasticsearch__  (version: 6.5.0) - persistent data source storage with advance search capabilities


### Prerequisites

1. _docker_ and _docker-compose_ installed  
2. In the _docker-compose.yaml_: set/update the absolute path to _elasticsearch:esdata1_ volume  
or remove the volume completely (used as a workaround for low storage space in the root partition).

### Installation

From the project's root:  

1). Clone dependent BitBucket repositories.  
```bash
        ./gradlew clone
```
2). Clean, build, and copy jar artifacts to shared docker volume.  
```bash
        ./gradlew clean build
        ./gradlew copyJars
```
3). Build docker images.  
```bash
        docker-compose build
```
4). Run all services (elasticsearch, spark cluster, ds-manager, ds-explorer) with docker-compose.  
```bash
       docker-compose up
```
5). (Optional) By default, spark cluster spawns 1 master + 1 worker. You can scale the cluster by adding additional workers in the docker-compose.yaml.  

Once all services are up and running, you can access:  

* DS-MANAGER API at http://localhost:8082
* DS-EXPLORER API at http://localhost:8084
* Spark master web UI at http://localhost:8080  
* Elasticsearch API at http://localhost:9200  

#### Logs
Application logs (ds-manager, ds-explorer) are available at the following location:
```text
    ds-builder/app-logs
```

Spark job execution logs are available in the Spark UI (http://spark-host:8080) only,    
since spark jobs are submitted via Spark REST API and there is no built-in mechanism to stream them back to the driver application.

For application-specific API examples refer to DS-MANAGER and DS-EXPLORER sections.

### Usage (basic flow)
To create a new data source from a given CSV file, drop the file into the shared data volume at
```text
    ds-builder/spark-docker/volumes/data
```  
and execute (insert valid DS and file names)  
```bash
    curl -XPOST -H "Content-Type: application/json" 'localhost:8082/api/v2/spark/jobs/ds/creator' -d '{
            "generateDs": false,
            "name": "{desired-ds-name}",
            "csvPath":"/opt/data/{csv-file-name}.csv",
            "header": true
        }'
``` 
as described in the [REST API usage and examples](https://bitbucket.org/goodzollder/ds-manager) section.  

Verify submitted job status using the link returned in the response.  
You can also monitor the execution status and/or worker execution logs in the Spark UI (http://your-host-name:8080).
  
Once the job successfully completes, the data source becomes available in the Elasticsearch.  
Run one of the two commands to verify it was created:  
```bash
    curl http://localhost:8084/api/v1/es/ds/all
    curl http://localhost:8084/api/v1/es/ds/search?index={desired-ds-name}
``` 

Repeat the steps above to create another data source.  
Use the "join" API example available in the [ds-manager](https://bitbucket.org/goodzollder/ds-manager) document to join created data sources.


# DS-MANAGER

Refer to [ds-manager](https://bitbucket.org/goodzollder/ds-manager) sub-project for details

# DS-EXPLORER

Refer to [ds-explorer](https://bitbucket.org/goodzollder/ds-explorer) sub-project for details

# CSV-LOADER

Refer to [csv-loader](https://bitbucket.org/goodzollder/csv-loader) sub-project for details

# DS-JOINER

Refer to [ds-joiner](https://bitbucket.org/goodzollder/ds-joiner) sub-project for details
